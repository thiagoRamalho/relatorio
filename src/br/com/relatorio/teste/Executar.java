package br.com.relatorio.teste;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import br.com.relatorio.Relatorio;
import br.com.relatorio.teste.mock.CreateInformation;
import br.com.relatorio.teste.mock.Player;

public class Executar {
	
	
	public static void main(String[] args) {

		try {

			//cria massa de dados para exibição	
			List<Player> collectionPlayers = CreateInformation.create();	
			
			//setar true ou false para visualizar o relatório em outro idioma
			boolean isPortuguese = false;

			//caminho relatório 
			String path = System.getProperty("user.dir")+"\\src\\br\\com\\relatorio\\recursos\\";
			String layout = "report1.jasper";
			
			//seta os subRelatórios...
			Map<String, String> subRelatorio = new HashMap<String, String>();
			subRelatorio.put("SUB_REPORT_DIR", "subReport1.jasper");
			
			//arquivo de internacionalização
			String baseName = "br.com.relatorio.recursos.messages";

			Locale myLoc = null;

			ResourceBundle bundle = null;

			if(isPortuguese){					
				myLoc = new Locale("pt", "BR");					
				bundle = ResourceBundle.getBundle(baseName, myLoc);
			} 
			else {
				myLoc = new Locale("en", "US");					
				bundle = ResourceBundle.getBundle(baseName, myLoc);		
			}
			
			Relatorio relatorio = new Relatorio(layout, path, bundle, subRelatorio);
			relatorio.adicionarDados(collectionPlayers);
			relatorio.criarRelatorio();
			relatorio.exibirRelatorio();
			

		} catch (Exception e) {
			e.printStackTrace();
		}	

	}
	
}
