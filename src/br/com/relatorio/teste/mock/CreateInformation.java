package br.com.relatorio.teste.mock;

import java.util.ArrayList;
import java.util.List;

/**
 * @author       $h@rk
 * @Date         19/11/2010
 * @project      ReportExample
 * @Description: Classe com o prop�sito de criar massa de dados 
 * 				 para testes
 */
public class CreateInformation {
	
	
	public static List<Player> create(){
		//create a player 
		Player player = new Player();
		
		player.setAge("27");
		player.setConsole("Playstation 3");
		player.setName("Player 1");
		player.setCountry("Brazil");
		
		//player's games
		Games games = new Games();
		
		games.setTitle("Uncharted II");
		games.setGenre("Adventure");
		
		Games games2 = new Games();
		
		games2.setTitle("God of War III");
		games2.setGenre("action/adventure (hack and slash)");
		
		//set de favorite game in collection
		player.addGames(games);
		player.addGames(games2);
		
		//create a player 
		Player player2 = new Player();
		
		player2.setAge("26");
		player2.setConsole("X-Box 360");
		player2.setName("Player 2");
		player2.setCountry("USA");
		
		//create the player's collectionGames
		
		Games games3 = new Games();
		
		games3.setTitle("Guitar Hero III - Legends of Rock");
		games3.setGenre("music");
		
		Games games4 = new Games();
		
		games4.setTitle("Call of Duty - Modern Warfare II");
		games4.setGenre("shooter/FPS");
		
		//set de favorite game in collection
		player2.addGames(games3);
		player2.addGames(games4);
		player2.addGames(new Games("Batman Arkhan Asylum", "Action"));
		
		Player player3 = new Player();
		player3.setAge("21");
		player3.setConsole("WII");
		player3.setCountry("Espan�");
		player3.setName("Player 3");
		
		
		player3.addGames(new Games("New Super Mario Bros", "Adventure"));
		player3.addGames(new Games("The Legend of Zelda", "Action/RPG"));
		player3.addGames(new Games("Wii Resort", "Mini-Games"));
		
		List<Player> collectionPlayers = new ArrayList<Player>();
		
		collectionPlayers.add(player2);
		collectionPlayers.add(player);
		collectionPlayers.add(player3);
		
		return collectionPlayers;
	}
}
