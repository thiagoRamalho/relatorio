package br.com.relatorio.teste.mock;

import java.util.ArrayList;
import java.util.List;

public class Player {
	
	private String name;
	
	private String age;
	
	private String console;
	
	private String country;
	
	private List<Games> collectionGames;
	
	public Player(){
		this.collectionGames = new ArrayList<Games>();
	}
	
	public void addGames(Games games){
		this.collectionGames.add(games);
	}	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getConsole() {
		return console;
	}
	public void setConsole(String console) {
		this.console = console;
	}
	public List<Games> getCollectionGames() {
		return collectionGames;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getCountry() {
		return country;
	}
	
}
