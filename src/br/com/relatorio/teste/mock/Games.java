package br.com.relatorio.teste.mock;

public class Games {
	
	public Games(){}
	
		
	public Games(String title, String genre) {
		super();
		this.title = title;
		this.genre = genre;
	}

	private String title;
	
	private String genre;
	
	
	public void setTitle(String title) {
		this.title = title;
	}
	public String getTitle() {
		return title;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	public String getGenre() {
		return genre;
	}
		
}
