package br.com.relatorio;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.ResourceBundle;
import java.util.Set;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;


/**
 * @author       $h@rk
 * @Date         18/02/2012
 * @project      Relatorio
 * @Description: 
 */
public class Relatorio {
	
	private String relatorio;
	private Map<String, String> subRelatorio;
	private String diretorioRelatorios;
	private ResourceBundle resourceBundle;
	private Collection<?> colecaoDados;
	private JasperPrint print;
	
	/**
	 * @param relatorio  relat�rio principal
	 * @param diretorioRelatorios caminho onde os arquivos *.jasper est�o
	 * @param resourceBundle objeto de internacionaliza��o
	 * @param subRelatorio cole��o de sub-relat�rios caso existam
	 */
	public Relatorio(String relatorio, String diretorioRelatorios,
			ResourceBundle resourceBundle, Map<String, String> subRelatorio) {
		super();
		this.relatorio = relatorio;
		this.subRelatorio = subRelatorio;
		this.diretorioRelatorios = diretorioRelatorios;
		this.resourceBundle = resourceBundle;		
	}
	
	/**
	 * Cole��o de dados que ser�o impressos
	 * 
	 * @param colecaoDados
	 */
	public void adicionarDados(Collection<?> colecaoDados){
		this.colecaoDados = colecaoDados;
	}
	
	/**
	 * Cria o relat�rio com base nas informa��es enviadas
	 * @throws JRException 
	 */
	public void criarRelatorio() throws JRException{
		
		//validamos os par�metros enviados
		this.validarParametros();
		
		JRBeanCollectionDataSource dataSource = null;

		//cria data source para manipula��o
		dataSource = new JRBeanCollectionDataSource(this.colecaoDados);

		String caminhoRelatorio = this.diretorioRelatorios.concat(this.relatorio);
		
		Map<String, Object> parametrosRelatorio = this.configuraParametrosRelatorio();
		
		//cria relat�rio e seta no atributo para posterior manipula��o
		this.print = JasperFillManager.fillReport(caminhoRelatorio, parametrosRelatorio, dataSource);		
	}
	
	/**
	 * Configura par�metros do sub-relat�rio
	 * @return
	 */
	private Map<String, Object> configuraParametrosRelatorio() {
		
		Map<String, Object>  parametros = new HashMap<String, Object>();
		
		//setamos o resource de internacionaliza��o
		parametros.put("REPORT_RESOURCE_BUNDLE", this.resourceBundle);
		
		//setamos o caminho dos subrelat�rios caso existam		
		if(this.subRelatorio != null){
			
			Set<Entry<String, String>> entrySet = this.subRelatorio.entrySet();
			
			//a chave no map representa o nome do par�metro no relat�rio
			for (Entry<String, String> entry : entrySet) {
				parametros.put(entry.getKey(), this.diretorioRelatorios+this.subRelatorio.get(entry.getKey()));
			}
		}		
		
		return parametros;
	}

	/**
	 * Exibe o relat�rio configurado
	 */
	public void exibirRelatorio(){
		
		//objeto respons�vel por exibir relat�rio (utiliza preview)	
		JasperViewer viewer = new JasperViewer(this.print);
		
		String tituloVersao = this.resourceBundle.getString("title");
		tituloVersao+= " - ";
		tituloVersao += this.resourceBundle.getString("versao");
		
		//pega o t�tulo do relat�rio e sua vers�o
		viewer.setTitle(tituloVersao);
		
		viewer.setDefaultCloseOperation(JasperViewer.EXIT_ON_CLOSE);
		
		viewer.setVisible(true);
	}
	
	private void validarParametros(){
		
		boolean isOk = true;
		String errorMessage = "";
		
		//se n�o enviou resource lan�amos exception
		if(this.resourceBundle == null){
			errorMessage = "resourceBundle n�o pode ser nulo";
			isOk = false;
		}
		
		//caso a cole��o de dados seja nula
		//ou vazia retorna exce��o pois n�o
		//ser� poss�vel criar o relat�rio
		if(this.colecaoDados == null || this.colecaoDados.isEmpty()){
			errorMessage = "cole��o de dados vazia ou nula";
			isOk = false;
		}
		
		//verifica se enviou caminho do diret�rio
		if(this.diretorioRelatorios == null || this.diretorioRelatorios.isEmpty()){
			errorMessage = "diret�rio de relat�rios inv�lido";
			isOk = false;
		}
		
		//verifica se enviou caminho do diret�rio
		if(this.relatorio == null || this.relatorio.isEmpty()){
			errorMessage = "relat�rio inv�lido";
			isOk = false;
		}
		
		//caso algum dos par�metros seja inv�lido, lan�a
		//exce��o
		if(!isOk){
			throw new IllegalArgumentException(errorMessage);			
		}		
	}
	
}
